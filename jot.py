from lark import Lark, Transformer


class JotTransformer(Transformer):

    @staticmethod
    def title(t):
        return {'title': t[0].value.strip()}

    @staticmethod
    def body(t):
        return {'body': t}

    @staticmethod
    def date(item):
        return {'date': item[0].value.strip()}

    @staticmethod
    def before(item):
        return {'before': item[0].value.strip()}

    @staticmethod
    def after(item):
        return {'after': item[0].value.strip()}

    @staticmethod
    def author(item):
        return {'author': item[0].value.strip()}

    @staticmethod
    def paragraph(items):
        return {'paragraph': ' '.join([i.value.strip('\n').strip() for i in items])}

    @staticmethod
    def start(items):
        return dict((key, item[key]) for item in items for key in item)


class Jot:
    def __init__(self):
        grammar = r"""
            start: _block+
            _block: title | author | date | body | before | after | _BLANK_LINE
            ?body: paragraph*
            title:  "-title" TEXT 
            author: "-by" TEXT
            date:   "-on" TEXT
            before: "-before" TEXT
            after:  "-after" TEXT
            paragraph: _BLANK_LINE LINE+
            TEXT: /(?!\-).+/
            LINE: /(?!\-).+/ /\n/?
            _BLANK_LINE: /\s*\n/
                        
            %import common.WS_INLINE
            %ignore WS_INLINE
        """
        self.parser = Lark(grammar)
        self.transformer = JotTransformer()

    def parse(self, text):
        tree = self.parser.parse(text)
        return self.transformer.transform(tree)


if __name__ == '__main__':
    test = '''
-title hello
-by tony
-on 2018-09-19
-before Chapter 3
-after  Chapter 1

This is the paragraph I am writing.
Jot is a mark up language.

Another paragraph
hello darkness my old friend
    '''
    out = Jot().parse(test)
    print(out)
