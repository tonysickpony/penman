from pathlib import Path
from shutil import copy

from attr import attrib, attrs
from slugify import slugify
from pyphen import Pyphen

from element import *
from jot import Jot

style: str = '''
.display-block {
    display: block;
}

.c-primary {
    color: #F2798A;
}

.pill {
    border-radius: 999px;
}

.flex-center {
    display: flex;
    justify-content: center;
}

.display-inline-block {
    display: inline-block;
}

.fit-width {
    width: fit-content;
}

.p-small {
    padding: 20px;
}

.dark-gray {
 color: #1C140D;
}

.light-gray {
 color: rgb(173, 173, 173);
}

.bg-light {
  background: #F2E9E1;
}

.m-y-0 {
    margin-top: 0;
    margin-bottom: 0;
}

.m-y-small {
    margin-top: 20px;
    margin-bottom: 20px;
}

.m-t-small {
    margin-top: 20px;
}

.m-b-small {
    margin-bottom: 20px;
}

.m-b-large {
    margin-bottom: 80px;
}

.m-x-small {
    margin-left: 20px;
    margin-right: 20px;
}

.m-x-auto {
    margin-left: auto;
    margin-right: auto;
}
.f-jumbo {
    font-size: 60px;
}
.f-44 {
    font-size: 44px;
}
.f-20 {
    font-size: 20px;
}

.f-16 {
    font-size: 16px;
    line-height: 24px;
}

.shadow {
    box-shadow: rgba(0, 0, 0, 0.15) 0px 6px 46px -10px;
}

.shadow:hover {
    box-shadow: rgba(0, 0, 0, 0.15) 0px 6px 52px -16px;
}

.ff-title {
    font-family: 'Copse';
}

.ff-brand {
    font-family: 'Abril Fatface';
}

body {
    font-family: 'Bitter', serif;
    max-width: 750px;
    margin: 0 auto;
    padding: 1rem;
}
p {
    font-size: 1.6rem;
    line-height: 2.5rem;
    word-spacing: 4px;
}

a {
    color: #000;
    text-decoration: none;
}


h1,h2,h3,h4,h5,h6 {
    margin: 0;
}
'''


@attrs
class Page:
    site_name = attrib(init=False)
    body = attrib(init=False)
    destination = attrib(init=False)

    @property
    def html(self):
        return Html(
            Head(
                Title(self.site_name),
                Stylesheet('https://fonts.googleapis.com/css?family=Bitter:400,400i,700'),
                Stylesheet('https://fonts.googleapis.com/css?family=Copse'),
                Stylesheet('https://fonts.googleapis.com/css?family=Bungee'),
                Stylesheet('https://fonts.googleapis.com/css?family=Abril+Fatface'),
                Style(style),
            ),
            Body(
                Header(
                    A(
                        Heading1(self.site_name.upper())
                    ).with_href('/').with_class('c-primary ').with_class('f-jumbo').with_class('ff-brand')
                ),
                self.body
            )
        ).with_lang('en').html

    def build(self):
        if not self.destination.parent.exists():
            self.destination.parent.mkdir(parents=True, exist_ok=True)
        self.destination.write_text(self.html)


@attrs
class Site:
    name = attrib()
    source = attrib()
    articles = attrib(factory=dict)
    collections = attrib(factory=dict)

    def __attrs_post_init__(self):
        self.site_root = self.source / 'public'
        self.site_url = Path('/')

    def scan(self):
        for path in self.source.glob('*.jot'):
            article = Article(
                name=path.stem,
                source=path,
                site_root=self.site_root,
                site_url=self.site_url,
                site_name=self.name
            )
            self.articles.update({path.stem: article})
            if self.collections.get('all') is None:
                self.collections['all'] = Collection(
                    name='all',
                    site_root=self.site_root,
                    site_url=self.site_url,
                    site_name=self.name
                )

            self.collections.get('all').add_article(article)

    def build_articles(self):
        self.site_root.mkdir(parents=True, exist_ok=True)
        for article in self.articles.values():
            article.build()

    def build_collections(self):
        for collection in self.collections.values():
            collection.build()

    def build_homepage(self):
        src = self.collections.get('all').collection_root / 'index.html'
        destination = self.site_root / 'index.html'
        copy(src, destination)


@attrs
class Collection:
    name = attrib()
    site_root = attrib()
    site_name = attrib()
    site_url = attrib()
    articles_per_page = attrib(default=3)
    articles = attrib(factory=list)

    def __attrs_post_init__(self):
        self.slug = slugify(self.name)
        self.collection_root = self.site_root / self.slug
        self.collection_url = self.site_url / self.slug

    def add_article(self, article):
        self.articles.append(article)

    @property
    def pages(self):
        step = self.articles_per_page
        starts = range(0, len(self.articles), step)
        total_pages = len(starts)
        for index, begin in enumerate(starts, start=1):
            page_articles = self.articles[begin:begin + step]
            yield ArticleList(
                articles=page_articles,
                index=index,
                collection_root=self.collection_root,
                collection_url=self.collection_url,
                site_root=self.site_root,
                site_url=self.site_url,
                site_name=self.site_name,
                total_pages=total_pages
            )

    def build(self):
        self.collection_root.mkdir(parents=True, exist_ok=True)
        for page in self.pages:
            page.build()
        src = self.collection_root / '1' / 'index.html'
        dest = self.collection_root / 'index.html'
        copy(src, dest)


@attrs
class Article(Page):
    name = attrib()
    source = attrib()
    site_name = attrib()
    site_root = attrib()
    site_url = attrib()

    def __attrs_post_init__(self):
        dic = Pyphen(lang='en')
        self.article_root = self.site_root / self.name
        self.article_url = self.site_url / self.name
        article = Jot().parse(self.source.read_text())
        self.author = article.get('author')
        self.title = article.get('title')
        self.date = article.get('date')

        def paragraphs(article_model):
            for part in article_model.get('body'):
                paragraph = part.get('paragraph')
                yield ' '.join([dic.inserted(word, hyphen='&shy;') for word in paragraph.split()])

        self.body = Main(
            Heading2(self.title).with_class('m-t-small').with_class('m-b-large').with_class('ff-title').with_class('f-44'),
            P(self.author).with_class('f-16'),
            Time(self.date).with_class('f-16').with_class('light-gray'),
            *[P(p) for p in paragraphs(article)]
        ).with_class('dark-gray')
        self.destination = self.article_root / 'index.html'


@attrs
class ArticleList(Page):
    articles = attrib()
    index = attrib()
    total_pages = attrib()
    site_root = attrib()
    site_url = attrib()
    site_name = attrib()
    collection_root = attrib()
    collection_url = attrib()

    def __attrs_post_init__(self):
        self.article_list_url = self.collection_url / str(self.index)
        self.article_list_root = self.collection_root / str(self.index)
        self.previous_url = self.collection_url / str(self.index - 1)
        self.next_url = self.collection_url / str(self.index + 1)
        self.destination = self.article_list_root / 'index.html'

        def render_article(article):
            article_link = Heading2(article.title).with_class('dark-gray').with_class('m-b-small').with_class('ff-title')
            article_author = P(article.author).with_class('dark-gray').with_class('m-y-0').with_class('f-16')
            article_meta = P(Text('Published at '), Time(article.date)).with_class('light-gray').with_class(
                'm-y-0').with_class('f-16')
            return Section(
                A(article_link, article_author, article_meta).with_href(article.article_url)
            ).with_class('shadow').with_class('p-small').with_class('m-y-small')

        self.body = Main(*[render_article(article) for article in self.articles], self.pagination())

    def pagination(self):
        previous_link = A('Previous').with_href(self.previous_url) if self.has_previous() else A('Previous')
        next_link = A('Next').with_href(self.next_url) if self.has_next() else A('Next')
        return Section(
            Span(previous_link),
            Span(f'{self.index} of {self.total_pages}').with_class('m-x-small'),
            Span(next_link),
        ).with_class('m-y-small').with_class('m-x-auto').with_class('display-block').with_class('f-20').with_class(
            'shadow').with_class('fit-width').with_class('p-small').with_class('pill')

    def has_previous(self):
        return self.index > 1

    def has_next(self):
        return self.index < self.total_pages


site = Site(name='Test Site', source=Path('.'))
site.scan()
site.build_articles()
site.build_collections()
site.build_homepage()
