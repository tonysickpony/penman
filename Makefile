requirements.txt:
	@echo "Generate $@"
	pipenv run pip freeze > $@

python-builder-image: requirements.txt
	@echo "Build docker image"
	docker build --tag=$@ $$(pwd)
	rm $<

python-builder-container: python-builder-image
	@echo "Build exe file from container"
	docker run --name=$@ $<

penman: python-builder-container
	@echo "Copy exe from container"
	docker cp $<:/root/dist/main $@
	docker rm -vf $<
